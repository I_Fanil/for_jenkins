package steps;

import io.cucumber.java.ParameterType;
import io.cucumber.java.ru.И;
import io.cucumber.java.ru.Пусть;
import io.cucumber.java.ru.Тогда;
import io.qameta.allure.Attachment;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.ashot.AShot;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.testng.Assert.assertTrue;

public class StepsFindPrinter {
    static WebDriver driver;
    static WebElement webElement;
    static WebDriverWait wait;


    @ParameterType(".*")
    public Categories categories(String categ) {
        return Categories.valueOf(categ);
    }

    @ParameterType(".*")
    public Sorting sorting(String sort) {
        return Sorting.valueOf(sort);
    }

    @Пусть("открыт ресурс авито")
    public static void openAvito() {
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, SECONDS);
        driver.get("https://www.avito.ru/");
        saveScreenshot();
    }

    @И("в выпадающем списке категорий выбрана {categories}")
    public static void chooseCategory(Categories category) {
        Select categorySelect = new Select(driver
                .findElement(By.cssSelector("#category")));
        categorySelect.selectByVisibleText(category.id);
        saveScreenshot();
    }

    @И("активирован чекбокс только с фотографией")
    public static void activateCheckBox() {
        wait.until(webDriver ->
                ((JavascriptExecutor) webDriver)
                        .executeScript("return document.readyState")
                        .equals("complete")
        );
        WebElement checkBox = driver.findElement(By.xpath(
                "//input[@name='withImagesOnly']/../span"));
        if (!checkBox.isSelected()) {
            checkBox.click();
        }
        saveScreenshot();
    }

    @И("в поле поиска введено значение {word}")
    public static void enterKind(String str) {
        webElement = driver.findElement(
                By.xpath("//input[@data-marker='search-form/suggest']"));
        webElement.click();
        webElement.sendKeys(str);
        saveScreenshot();
    }

    @Тогда("кликнуть по выпадающему списку региона")
    public static void chooseRegion() {
        driver.findElement(
                By.xpath("//div[@data-marker='search-form/region']")).click();
        saveScreenshot();
    }

    @Тогда("в поле регион введено значение {word}")
    public static void enterCity(String str) {
        driver.findElement(By.xpath(
                "//input[@data-marker='popup-location/region/input']"))
                .sendKeys(str);
        webElement = wait.until(
                ExpectedConditions.presenceOfElementLocated(
                        By.xpath("//li[@data-marker='suggest(0)']")));
        wait.until(ExpectedConditions
                .textToBePresentInElement(webElement, str));
        webElement.click();
        saveScreenshot();
    }

    @И("нажата кнопка Показать объявления")
    public static void clickShowButton() {
        driver.findElement(By.xpath(
                "//button[@data-marker='popup-location/save-button']"))
                .click();
        saveScreenshot();
    }

    @Тогда("открылась страница Результаты по запросу {word}")
    public static void allSearchResults(String str) {
        String header = driver.findElement(By.xpath("//*[@data-marker='page-title/text']")).getText();
        assertTrue(header.contains(str));
        saveScreenshot();
    }

    @И("в выпадающем списке сортировка выбрано значение {sorting}")
    public static void chooseSorting(Sorting sorting) {
        Select sortSelect = new Select(driver
                .findElement(By.xpath("//select[not(@id='category')]")));
        sortSelect.selectByVisibleText(sorting.id);
        saveScreenshot();
    }

    @И("в консоль выведено значение названия и цены {int} первых товаров")
    public static void finalResults(int num) {
        List<WebElement> list = driver
                .findElements(By.xpath(
                        "//div[@data-marker='catalog-serp']/div[@data-item-id]"));
        for (int i=0; i < num; i++) {
            System.out.println(list.get(i)
                    .findElement(By.cssSelector("[itemprop='name']"))
                    .getText());
            System.out.print(list.get(i)
                    .findElement(By.cssSelector("[itemprop='price']"))
                    .getAttribute("content") + " ");
            System.out.println(list.get(i)
                    .findElement(By.cssSelector("[itemprop='priceCurrency']"))
                    .getAttribute("content") + "\n");
        }
        saveScreenshot();
    }

    @Attachment(value = "Page screenshot", type = "image/png")
    public static byte[] saveScreenshot() {
        try {
            return makeScreenshot();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] makeScreenshot() throws IOException {
        BufferedImage image = new AShot()
                .takeScreenshot(driver)
                .getImage();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(image, "png", bos);
        bos.flush();
        byte[] imageByte = bos.toByteArray();
        bos.close();
        return imageByte;
    }
}
