#language: ru
Функционал: Поиск на авито с помощью datatable
  Структура сценария: Найдем самый дорогой <товар> на авито
    Пусть открыт ресурс авито
    И в выпадающем списке категорий выбрана оргтехника
    И активирован чекбокс только с фотографией
    И в поле поиска введено значение <товар>
    Тогда кликнуть по выпадающему списку региона
    Тогда в поле регион введено значение <город>
    И нажата кнопка Показать объявления
    Тогда открылась страница Результаты по запросу <товар>
    И в выпадающем списке сортировка выбрано значение Дороже
    И в консоль выведено значение названия и цены <кол-во объявлений> первых товаров

    Примеры:
      | товар   | город  | кол-во объявлений |
      | сканер  | Уфа    | 5                 |
      | плоттер | Казань | 2                 |
      | МФУ     | Москва | 4                 |